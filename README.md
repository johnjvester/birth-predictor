# `birth-predictor` Repository

[![pipeline status](https://gitlab.com/johnjvester/birth-predictor/badges/master/pipeline.svg)](https://gitlab.com/johnjvester/birth-predictor/commits/master)


> The `birth-predictor` repository contains a [Spring Boot](https://spring.io/projects/spring-boot) RESTful API service
> that provides birth gender predictions using the [Chinese Gender Predictor](https://www.thebump.com/chinese-gender-chart) 
> via a simple POST request.

## Publications

This repository is related to a DZone.com publication:

* [Purpose-Driven Microservice Design](https://dzone.com/articles/purpose-driven-microservice-design)

To read more of my publications, please review one of the following URLs:

* https://dzone.com/users/1224939/johnjvester.html
* https://johnjvester.gitlab.io/dZoneStatistics/WebContent/#/stats?id=1224939

## Starting the `birth-predictor` Repository

### Locally

Simply run the `birth-predictor` Spring Boot service by using the instructions listed below:

https://docs.spring.io/spring-boot/docs/1.5.16.RELEASE/reference/html/using-boot-running-your-application.html

### Docker

By using the `Dockerfile` included in this project, the file will perform a Maven build, then establish the `birth-predictor` 
image.  Once created, the `birth-predictor` service can be started using `docker run`: 

```shell
docker build -t birth-predictor . 
docker run -p 8585:8585 birth-predictor 
```

### Service Console

Once started, the following console information should appear:

```shell

888      d8b         888    888                                         888 d8b          888
888      Y8P         888    888                                         888 Y8P          888
888                  888    888                                         888              888
88888b.  888 888d888 888888 88888b.       88888b.  888d888 .d88b.   .d88888 888  .d8888b 888888 .d88b.  888d888
888 "88b 888 888P"   888    888 "88b      888 "88b 888P"  d8P  Y8b d88" 888 888 d88P"    888   d88""88b 888P"
888  888 888 888     888    888  888      888  888 888    88888888 888  888 888 888      888   888  888 888
888 d88P 888 888     Y88b.  888  888      888 d88P 888    Y8b.     Y88b 888 888 Y88b.    Y88b. Y88..88P 888
88888P"  888 888      "Y888 888  888      88888P"  888     "Y8888   "Y88888 888  "Y8888P  "Y888 "Y88P"  888
                                          888
                                          888
                                          888

:: Birth Predictor RESTful API :: Running Spring Boot 2.7.0 :: Port #8585 :: 

2022-05-30 08:15:25.216  INFO 60110 --- [           main] c.g.j.b.BirthPredictorApplication        : Starting BirthPredictorApplication using Java 11.0.15 on mq-m-jvester with PID 60110 (/Users/jvester/projects/jvc/birth-predictor/target/classes started by jvester in /Users/jvester/projects/jvc/birth-predictor)
2022-05-30 08:15:25.219  INFO 60110 --- [           main] c.g.j.b.BirthPredictorApplication        : No active profile set, falling back to 1 default profile: "default"
2022-05-30 08:15:26.688  INFO 60110 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat initialized with port(s): 8585 (http)
2022-05-30 08:15:26.695  INFO 60110 --- [           main] o.apache.catalina.core.StandardService   : Starting service [Tomcat]
2022-05-30 08:15:26.695  INFO 60110 --- [           main] org.apache.catalina.core.StandardEngine  : Starting Servlet engine: [Apache Tomcat/9.0.63]
2022-05-30 08:15:26.791  INFO 60110 --- [           main] o.a.c.c.C.[Tomcat].[localhost].[/]       : Initializing Spring embedded WebApplicationContext
2022-05-30 08:15:26.791  INFO 60110 --- [           main] w.s.c.ServletWebServerApplicationContext : Root WebApplicationContext: initialization completed in 1529 ms
2022-05-30 08:15:27.953  INFO 60110 --- [           main] o.s.b.a.e.web.EndpointLinksResolver      : Exposing 1 endpoint(s) beneath base path '/actuator'
2022-05-30 08:15:28.072  INFO 60110 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat started on port(s): 8585 (http) with context path ''
2022-05-30 08:15:28.107  INFO 60110 --- [           main] c.g.j.b.BirthPredictorApplication        : Started BirthPredictorApplication in 3.501 seconds (JVM running for 4.269)
```

## Using the `birth-predictor` Repository

The `birth-predictor` repository will perform the lookup against the table displayed below:

![Birth Predictor Grid](BirthPredictorGrid.png)

The following cURL command will request the expected gender for a child conceived in the 11th month (November) by a 
mother who is 43 years old at the time of conception.

```shell
curl --location --request POST 'http://localhost:8585/predict' \
--header 'Content-Type: application/json' \
--data-raw '{
    "conceptionMonth" : 11,
    "conceptionAge" : 43
}'
```

The `birth-preditor` service will respond with a `202 Accepted` HTTP status code and the following payload:

```json
{
    "month": 11,
    "age": 43,
    "gender": "male",
    "errorMessage": null
}
```

The results successfully predicted the gender of my son, born in 2017.

### Swagger API Docs

For more information, please review the Swagger documentation at the following URL:

http://localhost:8585/swagger-ui/index.html#

## Additional Information

Made with <span style="color:red;">♥</span> &nbsp;by johnjvester@gmail.com, because I enjoy writing code.
