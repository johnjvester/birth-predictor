package com.gitlab.johnjvester.birthpredictor.services;

import com.gitlab.johnjvester.birthpredictor.models.BirthPredictorRequest;
import com.gitlab.johnjvester.birthpredictor.models.Prediction;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class BirthPredictorServiceTest {
    @Autowired
    BirthPredictorService birthPredictorService;

    @DisplayName("18-year-old predictions")
    @Test
    void test18() throws Exception {
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(1, 18)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(2, 18)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(3, 18)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(4, 18)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(5, 18)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(6, 18)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(7, 18)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(8, 18)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(9, 18)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(10, 18)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(11, 18)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(12, 18)).getGender());
    }

    @DisplayName("19-year-old predictions")
    @Test
    void test19() throws Exception {
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(1, 19)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(2, 19)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(3, 19)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(4, 19)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(5, 19)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(6, 19)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(7, 19)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(8, 19)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(9, 19)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(10, 19)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(11, 19)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(12, 19)).getGender());
    }

    @DisplayName("20-year-old predictions")
    @Test
    void test20() throws Exception {
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(1, 20)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(2, 20)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(3, 20)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(4, 20)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(5, 20)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(6, 20)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(7, 20)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(8, 20)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(9, 20)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(10, 20)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(11, 20)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(12, 20)).getGender());
    }

    @DisplayName("21-year-old predictions")
    @Test
    void test21() throws Exception {
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(1, 21)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(2, 21)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(3, 21)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(4, 21)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(5, 21)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(6, 21)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(7, 21)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(8, 21)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(9, 21)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(10, 21)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(11, 21)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(12, 21)).getGender());
    }

    @DisplayName("22-year-old predictions")
    @Test
    void test22() throws Exception {
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(1, 22)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(2, 22)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(3, 22)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(4, 22)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(5, 22)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(6, 22)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(7, 22)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(8, 22)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(9, 22)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(10, 22)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(11, 22)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(12, 22)).getGender());
    }

    @DisplayName("23-year-old predictions")
    @Test
    void test23() throws Exception {
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(1, 23)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(2, 23)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(3, 23)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(4, 23)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(5, 23)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(6, 23)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(7, 23)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(8, 23)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(9, 23)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(10, 23)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(11, 23)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(12, 23)).getGender());
    }

    @DisplayName("24-year-old predictions")
    @Test
    void test24() throws Exception {
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(1, 24)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(2, 24)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(3, 24)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(4, 24)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(5, 24)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(6, 24)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(7, 24)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(8, 24)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(9, 24)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(10, 24)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(11, 24)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(12, 24)).getGender());
    }

    @DisplayName("25-year-old predictions")
    @Test
    void test25() throws Exception {
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(1, 25)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(2, 25)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(3, 25)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(4, 25)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(5, 25)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(6, 25)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(7, 25)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(8, 25)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(9, 25)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(10, 25)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(11, 25)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(12, 25)).getGender());
    }

    @DisplayName("26-year-old predictions")
    @Test
    void test26() throws Exception {
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(1, 26)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(2, 26)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(3, 26)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(4, 26)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(5, 26)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(6, 26)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(7, 26)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(8, 26)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(9, 26)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(10, 26)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(11, 26)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(12, 26)).getGender());
    }

    @DisplayName("27-year-old predictions")
    @Test
    void test27() throws Exception {
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(1, 27)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(2, 27)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(3, 27)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(4, 27)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(5, 27)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(6, 27)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(7, 27)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(8, 27)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(9, 27)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(10, 27)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(11, 27)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(12, 27)).getGender());
    }

    @DisplayName("28-year-old predictions")
    @Test
    void test28() throws Exception {
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(1, 28)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(2, 28)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(3, 28)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(4, 28)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(5, 28)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(6, 28)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(7, 28)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(8, 28)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(9, 28)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(10, 28)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(11, 28)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(12, 28)).getGender());
    }

    @DisplayName("29-year-old predictions")
    @Test
    void test29() throws Exception {
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(1, 29)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(2, 29)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(3, 29)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(4, 29)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(5, 29)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(6, 29)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(7, 29)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(8, 29)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(9, 29)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(10, 29)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(11, 29)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(12, 29)).getGender());
    }

    @DisplayName("30-year-old predictions")
    @Test
    void test30() throws Exception {
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(1, 30)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(2, 30)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(3, 30)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(4, 30)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(5, 30)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(6, 30)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(7, 30)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(8, 30)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(9, 30)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(10, 30)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(11, 30)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(12, 30)).getGender());
    }

    @DisplayName("31-year-old predictions")
    @Test
    void test31() throws Exception {
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(1, 31)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(2, 31)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(3, 31)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(4, 31)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(5, 31)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(6, 31)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(7, 31)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(8, 31)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(9, 31)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(10, 31)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(11, 31)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(12, 31)).getGender());
    }

    @DisplayName("32-year-old predictions")
    @Test
    void test32() throws Exception {
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(1, 32)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(2, 32)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(3, 32)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(4, 32)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(5, 32)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(6, 32)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(7, 32)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(8, 32)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(9, 32)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(10, 32)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(11, 32)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(12, 32)).getGender());
    }

    @DisplayName("33-year-old predictions")
    @Test
    void test33() throws Exception {
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(1, 33)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(2, 33)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(3, 33)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(4, 33)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(5, 33)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(6, 33)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(7, 33)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(8, 33)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(9, 33)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(10, 33)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(11, 33)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(12, 33)).getGender());
    }

    @DisplayName("34-year-old predictions")
    @Test
    void test34() throws Exception {
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(1, 34)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(2, 34)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(3, 34)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(4, 34)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(5, 34)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(6, 34)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(7, 34)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(8, 34)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(9, 34)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(10, 34)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(11, 34)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(12, 34)).getGender());
    }

    @DisplayName("35-year-old predictions")
    @Test
    void test35() throws Exception {
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(1, 35)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(2, 35)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(3, 35)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(4, 35)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(5, 35)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(6, 35)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(7, 35)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(8, 35)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(9, 35)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(10, 35)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(11, 35)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(12, 35)).getGender());
    }

    @DisplayName("36-year-old predictions")
    @Test
    void test36() throws Exception {
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(1, 36)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(2, 36)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(3, 36)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(4, 36)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(5, 36)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(6, 36)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(7, 36)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(8, 36)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(9, 36)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(10, 36)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(11, 36)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(12, 36)).getGender());
    }

    @DisplayName("37-year-old predictions")
    @Test
    void test37() throws Exception {
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(1, 37)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(2, 37)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(3, 37)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(4, 37)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(5, 37)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(6, 37)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(7, 37)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(8, 37)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(9, 37)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(10, 37)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(11, 37)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(12, 37)).getGender());
    }

    @DisplayName("38-year-old predictions")
    @Test
    void test38() throws Exception {
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(1, 38)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(2, 38)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(3, 38)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(4, 38)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(5, 38)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(6, 38)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(7, 38)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(8, 38)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(9, 38)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(10, 38)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(11, 38)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(12, 38)).getGender());
    }

    @DisplayName("39-year-old predictions")
    @Test
    void test39() throws Exception {
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(1, 39)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(2, 39)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(3, 39)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(4, 39)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(5, 39)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(6, 39)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(7, 39)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(8, 39)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(9, 39)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(10, 39)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(11, 39)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(12, 39)).getGender());
    }

    @DisplayName("40-year-old predictions")
    @Test
    void test40() throws Exception {
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(1, 40)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(2, 40)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(3, 40)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(4, 40)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(5, 40)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(6, 40)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(7, 40)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(8, 40)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(9, 40)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(10, 40)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(11, 40)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(12, 40)).getGender());
    }

    @DisplayName("41-year-old predictions")
    @Test
    void test41() throws Exception {
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(1, 41)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(2, 41)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(3, 41)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(4, 41)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(5, 41)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(6, 41)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(7, 41)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(8, 41)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(9, 41)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(10, 41)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(11, 41)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(12, 41)).getGender());
    }

    @DisplayName("42-year-old predictions")
    @Test
    void test42() throws Exception {
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(1, 42)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(2, 42)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(3, 42)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(4, 42)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(5, 42)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(6, 42)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(7, 42)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(8, 42)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(9, 42)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(10, 42)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(11, 42)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(12, 42)).getGender());
    }

    @DisplayName("43-year-old predictions")
    @Test
    void test43() throws Exception {
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(1, 43)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(2, 43)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(3, 43)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(4, 43)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(5, 43)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(6, 43)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(7, 43)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(8, 43)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(9, 43)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(10, 43)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(11, 43)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(12, 43)).getGender());
    }

    @DisplayName("44-year-old predictions")
    @Test
    void test44() throws Exception {
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(1, 44)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(2, 44)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(3, 44)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(4, 44)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(5, 44)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(6, 44)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(7, 44)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(8, 44)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(9, 44)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(10, 44)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(11, 44)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(12, 44)).getGender());
    }

    @DisplayName("45-year-old predictions")
    @Test
    void test45() throws Exception {
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(1, 45)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(2, 45)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(3, 45)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(4, 45)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(5, 45)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(6, 45)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(7, 45)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(8, 45)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(9, 45)).getGender());
        assertEquals(Prediction.GENDER_FEMALE, birthPredictorService.predict(new BirthPredictorRequest(10, 45)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(11, 45)).getGender());
        assertEquals(Prediction.GENDER_MALE, birthPredictorService.predict(new BirthPredictorRequest(12, 45)).getGender());
    }

    @DisplayName("Testing Exceptions")
    @Test
    void testExceptions() {
        try {
            birthPredictorService.predict(new BirthPredictorRequest(0, 18));
            fail();
        } catch (Exception e) {
            if (e.getMessage().equals(BirthPredictorService.ERROR_INVALID_MONTH)) {
                assertTrue(true);
            } else {
                fail();
            }
        }

        try {
            birthPredictorService.predict(new BirthPredictorRequest(13, 18));
            fail();
        } catch (Exception e) {
            if (e.getMessage().equals(BirthPredictorService.ERROR_INVALID_MONTH)) {
                assertTrue(true);
            } else {
                fail();
            }
        }

        try {
            birthPredictorService.predict(new BirthPredictorRequest(1, 17));
            fail();
        } catch (Exception e) {
            if (e.getMessage().equals(BirthPredictorService.ERROR_INVALID_AGE)) {
                assertTrue(true);
            } else {
                fail();
            }
        }

        try {
            birthPredictorService.predict(new BirthPredictorRequest(1, 46));
            fail();
        } catch (Exception e) {
            if (e.getMessage().equals(BirthPredictorService.ERROR_INVALID_AGE)) {
                assertTrue(true);
            } else {
                fail();
            }
        }
    }
}
