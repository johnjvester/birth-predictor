package com.gitlab.johnjvester.birthpredictor.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.johnjvester.birthpredictor.models.BirthPredictorRequest;
import com.gitlab.johnjvester.birthpredictor.models.Prediction;
import com.gitlab.johnjvester.birthpredictor.services.BirthPredictorService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(BirthPredictorController.class)
public class BirthPredictorControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    BirthPredictorService birthPredictorService;

    @Test
    void testPredict() throws Exception {
        BirthPredictorRequest birthPredictorRequest = new BirthPredictorRequest(1, 18);
        when(birthPredictorService.predict(birthPredictorRequest)).thenReturn(new Prediction(1, 18, Prediction.GENDER_FEMALE));

        mockMvc.perform(post("/predict")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(birthPredictorRequest)))
                .andExpect(status().isAccepted());
    }

    @Test
    void testPredictException() throws Exception {
        BirthPredictorRequest birthPredictorRequest = new BirthPredictorRequest(1, 17);
        when(birthPredictorService.predict(birthPredictorRequest)).thenThrow(new Exception(BirthPredictorService.ERROR_INVALID_AGE));

        mockMvc.perform(post("/predict")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(birthPredictorRequest)))
                .andExpect(status().isBadRequest());
    }

    private static String asJsonString(BirthPredictorRequest birthPredictorRequest) {
        try {
            return new ObjectMapper().writeValueAsString(birthPredictorRequest);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
