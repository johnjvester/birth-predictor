package com.gitlab.johnjvester.birthpredictor;

import com.gitlab.johnjvester.birthpredictor.models.Prediction;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class BirthPredictorApplicationTests {

    @Test
    void contextLoads() {
    }
}
