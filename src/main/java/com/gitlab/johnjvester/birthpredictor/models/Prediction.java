package com.gitlab.johnjvester.birthpredictor.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Prediction {
    public static final String GENDER_FEMALE = "female";
    public static final String GENDER_MALE = "male";

    private int month;
    private int age;
    private String gender;
    private String errorMessage;

    public Prediction(int month, int age, String gender) {
        this.month = month;
        this.age = age;
        this.gender = gender;
    }
}
