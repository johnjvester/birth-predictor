package com.gitlab.johnjvester.birthpredictor.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class BirthPredictorRequest {
    private int conceptionMonth;
    private int conceptionAge;
}
