package com.gitlab.johnjvester.birthpredictor.services;

import com.gitlab.johnjvester.birthpredictor.models.BirthPredictorRequest;
import com.gitlab.johnjvester.birthpredictor.models.Prediction;
import com.gitlab.johnjvester.birthpredictor.utils.BirthPredictorUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class BirthPredictorService {
    public static final String ERROR_INVALID_MONTH = "Conception month must be between 1 and 12";
    public static final String ERROR_INVALID_AGE = "Conception age must be between 18 and 45";

    public Prediction predict(BirthPredictorRequest birthPredictorRequest) throws Exception {
        if (birthPredictorRequest.getConceptionMonth() < 1 || birthPredictorRequest.getConceptionMonth() > 12) {
            throw new Exception(ERROR_INVALID_MONTH);
        }

        if (birthPredictorRequest.getConceptionAge() < 18 || birthPredictorRequest.getConceptionAge() > 45) {
            throw new Exception(ERROR_INVALID_AGE);
        }

        Prediction prediction = BirthPredictorUtils.buildPredictions().stream()
                .filter(p -> p.getMonth() == birthPredictorRequest.getConceptionMonth() && p.getAge() == birthPredictorRequest.getConceptionAge())
                .findFirst()
                .orElseThrow(() -> new Exception("Could not find results for birthPredictorRequest=" + birthPredictorRequest));

        log.info("birthPredictorRequest={}, prediction={}", birthPredictorRequest, prediction);
        return prediction;
    }
}
