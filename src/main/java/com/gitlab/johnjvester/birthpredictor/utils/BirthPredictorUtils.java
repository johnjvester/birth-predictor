package com.gitlab.johnjvester.birthpredictor.utils;

import com.gitlab.johnjvester.birthpredictor.models.Prediction;

import java.util.ArrayList;
import java.util.List;

public final class BirthPredictorUtils {
    private BirthPredictorUtils() { }

    public static List<Prediction> buildPredictions() {
        List<Prediction> predictions = new ArrayList<>();

        predictions.add(new Prediction(1, 18, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(2, 18, Prediction.GENDER_MALE));
        predictions.add(new Prediction(3, 18, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(4, 18, Prediction.GENDER_MALE));
        predictions.add(new Prediction(5, 18, Prediction.GENDER_MALE));
        predictions.add(new Prediction(6, 18, Prediction.GENDER_MALE));
        predictions.add(new Prediction(7, 18, Prediction.GENDER_MALE));
        predictions.add(new Prediction(8, 18, Prediction.GENDER_MALE));
        predictions.add(new Prediction(9, 18, Prediction.GENDER_MALE));
        predictions.add(new Prediction(10, 18, Prediction.GENDER_MALE));
        predictions.add(new Prediction(11, 18, Prediction.GENDER_MALE));
        predictions.add(new Prediction(12, 18, Prediction.GENDER_MALE));

        predictions.add(new Prediction(1, 19, Prediction.GENDER_MALE));
        predictions.add(new Prediction(2, 19, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(3, 19, Prediction.GENDER_MALE));
        predictions.add(new Prediction(4, 19, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(5, 19, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(6, 19, Prediction.GENDER_MALE));
        predictions.add(new Prediction(7, 19, Prediction.GENDER_MALE));
        predictions.add(new Prediction(8, 19, Prediction.GENDER_MALE));
        predictions.add(new Prediction(9, 19, Prediction.GENDER_MALE));
        predictions.add(new Prediction(10, 19, Prediction.GENDER_MALE));
        predictions.add(new Prediction(11, 19, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(12, 19, Prediction.GENDER_FEMALE));

        predictions.add(new Prediction(1, 20, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(2, 20, Prediction.GENDER_MALE));
        predictions.add(new Prediction(3, 20, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(4, 20, Prediction.GENDER_MALE));
        predictions.add(new Prediction(5, 20, Prediction.GENDER_MALE));
        predictions.add(new Prediction(6, 20, Prediction.GENDER_MALE));
        predictions.add(new Prediction(7, 20, Prediction.GENDER_MALE));
        predictions.add(new Prediction(8, 20, Prediction.GENDER_MALE));
        predictions.add(new Prediction(9, 20, Prediction.GENDER_MALE));
        predictions.add(new Prediction(10, 20, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(11, 20, Prediction.GENDER_MALE));
        predictions.add(new Prediction(12, 20, Prediction.GENDER_MALE));

        predictions.add(new Prediction(1, 21, Prediction.GENDER_MALE));
        predictions.add(new Prediction(2, 21, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(3, 21, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(4, 21, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(5, 21, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(6, 21, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(7, 21, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(8, 21, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(9, 21, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(10, 21, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(11, 21, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(12, 21, Prediction.GENDER_FEMALE));

        predictions.add(new Prediction(1, 22, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(2, 22, Prediction.GENDER_MALE));
        predictions.add(new Prediction(3, 22, Prediction.GENDER_MALE));
        predictions.add(new Prediction(4, 22, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(5, 22, Prediction.GENDER_MALE));
        predictions.add(new Prediction(6, 22, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(7, 22, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(8, 22, Prediction.GENDER_MALE));
        predictions.add(new Prediction(9, 22, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(10, 22, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(11, 22, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(12, 22, Prediction.GENDER_FEMALE));

        predictions.add(new Prediction(1, 23, Prediction.GENDER_MALE));
        predictions.add(new Prediction(2, 23, Prediction.GENDER_MALE));
        predictions.add(new Prediction(3, 23, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(4, 23, Prediction.GENDER_MALE));
        predictions.add(new Prediction(5, 23, Prediction.GENDER_MALE));
        predictions.add(new Prediction(6, 23, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(7, 23, Prediction.GENDER_MALE));
        predictions.add(new Prediction(8, 23, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(9, 23, Prediction.GENDER_MALE));
        predictions.add(new Prediction(10, 23, Prediction.GENDER_MALE));
        predictions.add(new Prediction(11, 23, Prediction.GENDER_MALE));
        predictions.add(new Prediction(12, 23, Prediction.GENDER_FEMALE));

        predictions.add(new Prediction(1, 24, Prediction.GENDER_MALE));
        predictions.add(new Prediction(2, 24, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(3, 24, Prediction.GENDER_MALE));
        predictions.add(new Prediction(4, 24, Prediction.GENDER_MALE));
        predictions.add(new Prediction(5, 24, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(6, 24, Prediction.GENDER_MALE));
        predictions.add(new Prediction(7, 24, Prediction.GENDER_MALE));
        predictions.add(new Prediction(8, 24, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(9, 24, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(10, 24, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(11, 24, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(12, 24, Prediction.GENDER_FEMALE));

        predictions.add(new Prediction(1, 25, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(2, 25, Prediction.GENDER_MALE));
        predictions.add(new Prediction(3, 25, Prediction.GENDER_MALE));
        predictions.add(new Prediction(4, 25, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(5, 25, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(6, 25, Prediction.GENDER_MALE));
        predictions.add(new Prediction(7, 25, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(8, 25, Prediction.GENDER_MALE));
        predictions.add(new Prediction(9, 25, Prediction.GENDER_MALE));
        predictions.add(new Prediction(10, 25, Prediction.GENDER_MALE));
        predictions.add(new Prediction(11, 25, Prediction.GENDER_MALE));
        predictions.add(new Prediction(12, 25, Prediction.GENDER_MALE));

        predictions.add(new Prediction(1, 26, Prediction.GENDER_MALE));
        predictions.add(new Prediction(2, 26, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(3, 26, Prediction.GENDER_MALE));
        predictions.add(new Prediction(4, 26, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(5, 26, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(6, 26, Prediction.GENDER_MALE));
        predictions.add(new Prediction(7, 26, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(8, 26, Prediction.GENDER_MALE));
        predictions.add(new Prediction(9, 26, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(10, 26, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(11, 26, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(12, 26, Prediction.GENDER_FEMALE));

        predictions.add(new Prediction(1, 27, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(2, 27, Prediction.GENDER_MALE));
        predictions.add(new Prediction(3, 27, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(4, 27, Prediction.GENDER_MALE));
        predictions.add(new Prediction(5, 27, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(6, 27, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(7, 27, Prediction.GENDER_MALE));
        predictions.add(new Prediction(8, 27, Prediction.GENDER_MALE));
        predictions.add(new Prediction(9, 27, Prediction.GENDER_MALE));
        predictions.add(new Prediction(10, 27, Prediction.GENDER_MALE));
        predictions.add(new Prediction(11, 27, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(12, 27, Prediction.GENDER_MALE));

        predictions.add(new Prediction(1, 28, Prediction.GENDER_MALE));
        predictions.add(new Prediction(2, 28, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(3, 28, Prediction.GENDER_MALE));
        predictions.add(new Prediction(4, 28, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(5, 28, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(6, 28, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(7, 28, Prediction.GENDER_MALE));
        predictions.add(new Prediction(8, 28, Prediction.GENDER_MALE));
        predictions.add(new Prediction(9, 28, Prediction.GENDER_MALE));
        predictions.add(new Prediction(10, 28, Prediction.GENDER_MALE));
        predictions.add(new Prediction(11, 28, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(12, 28, Prediction.GENDER_FEMALE));

        predictions.add(new Prediction(1, 29, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(2, 29, Prediction.GENDER_MALE));
        predictions.add(new Prediction(3, 29, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(4, 29, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(5, 29, Prediction.GENDER_MALE));
        predictions.add(new Prediction(6, 29, Prediction.GENDER_MALE));
        predictions.add(new Prediction(7, 29, Prediction.GENDER_MALE));
        predictions.add(new Prediction(8, 29, Prediction.GENDER_MALE));
        predictions.add(new Prediction(9, 29, Prediction.GENDER_MALE));
        predictions.add(new Prediction(10, 29, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(11, 29, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(12, 29, Prediction.GENDER_FEMALE));

        predictions.add(new Prediction(1, 30, Prediction.GENDER_MALE));
        predictions.add(new Prediction(2, 30, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(3, 30, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(4, 30, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(5, 30, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(6, 30, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(7, 30, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(8, 30, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(9, 30, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(10, 30, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(11, 30, Prediction.GENDER_MALE));
        predictions.add(new Prediction(12, 30, Prediction.GENDER_MALE));

        predictions.add(new Prediction(1, 31, Prediction.GENDER_MALE));
        predictions.add(new Prediction(2, 31, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(3, 31, Prediction.GENDER_MALE));
        predictions.add(new Prediction(4, 31, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(5, 31, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(6, 31, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(7, 31, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(8, 31, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(9, 31, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(10, 31, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(11, 31, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(12, 31, Prediction.GENDER_MALE));

        predictions.add(new Prediction(1, 32, Prediction.GENDER_MALE));
        predictions.add(new Prediction(2, 32, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(3, 32, Prediction.GENDER_MALE));
        predictions.add(new Prediction(4, 32, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(5, 32, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(6, 32, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(7, 32, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(8, 32, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(9, 32, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(10, 32, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(11, 32, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(12, 32, Prediction.GENDER_MALE));

        predictions.add(new Prediction(1, 33, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(2, 33, Prediction.GENDER_MALE));
        predictions.add(new Prediction(3, 33, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(4, 33, Prediction.GENDER_MALE));
        predictions.add(new Prediction(5, 33, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(6, 33, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(7, 33, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(8, 33, Prediction.GENDER_MALE));
        predictions.add(new Prediction(9, 33, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(10, 33, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(11, 33, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(12, 33, Prediction.GENDER_MALE));

        predictions.add(new Prediction(1, 34, Prediction.GENDER_MALE));
        predictions.add(new Prediction(2, 34, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(3, 34, Prediction.GENDER_MALE));
        predictions.add(new Prediction(4, 34, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(5, 34, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(6, 34, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(7, 34, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(8, 34, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(9, 34, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(10, 34, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(11, 34, Prediction.GENDER_MALE));
        predictions.add(new Prediction(12, 34, Prediction.GENDER_MALE));

        predictions.add(new Prediction(1, 35, Prediction.GENDER_MALE));
        predictions.add(new Prediction(2, 35, Prediction.GENDER_MALE));
        predictions.add(new Prediction(3, 35, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(4, 35, Prediction.GENDER_MALE));
        predictions.add(new Prediction(5, 35, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(6, 35, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(7, 35, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(8, 35, Prediction.GENDER_MALE));
        predictions.add(new Prediction(9, 35, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(10, 35, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(11, 35, Prediction.GENDER_MALE));
        predictions.add(new Prediction(12, 35, Prediction.GENDER_MALE));

        predictions.add(new Prediction(1, 36, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(2, 36, Prediction.GENDER_MALE));
        predictions.add(new Prediction(3, 36, Prediction.GENDER_MALE));
        predictions.add(new Prediction(4, 36, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(5, 36, Prediction.GENDER_MALE));
        predictions.add(new Prediction(6, 36, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(7, 36, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(8, 36, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(9, 36, Prediction.GENDER_MALE));
        predictions.add(new Prediction(10, 36, Prediction.GENDER_MALE));
        predictions.add(new Prediction(11, 36, Prediction.GENDER_MALE));
        predictions.add(new Prediction(12, 36, Prediction.GENDER_MALE));

        predictions.add(new Prediction(1, 37, Prediction.GENDER_MALE));
        predictions.add(new Prediction(2, 37, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(3, 37, Prediction.GENDER_MALE));
        predictions.add(new Prediction(4, 37, Prediction.GENDER_MALE));
        predictions.add(new Prediction(5, 37, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(6, 37, Prediction.GENDER_MALE));
        predictions.add(new Prediction(7, 37, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(8, 37, Prediction.GENDER_MALE));
        predictions.add(new Prediction(9, 37, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(10, 37, Prediction.GENDER_MALE));
        predictions.add(new Prediction(11, 37, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(12, 37, Prediction.GENDER_MALE));

        predictions.add(new Prediction(1, 38, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(2, 38, Prediction.GENDER_MALE));
        predictions.add(new Prediction(3, 38, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(4, 38, Prediction.GENDER_MALE));
        predictions.add(new Prediction(5, 38, Prediction.GENDER_MALE));
        predictions.add(new Prediction(6, 38, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(7, 38, Prediction.GENDER_MALE));
        predictions.add(new Prediction(8, 38, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(9, 38, Prediction.GENDER_MALE));
        predictions.add(new Prediction(10, 38, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(11, 38, Prediction.GENDER_MALE));
        predictions.add(new Prediction(12, 38, Prediction.GENDER_FEMALE));

        predictions.add(new Prediction(1, 39, Prediction.GENDER_MALE));
        predictions.add(new Prediction(2, 39, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(3, 39, Prediction.GENDER_MALE));
        predictions.add(new Prediction(4, 39, Prediction.GENDER_MALE));
        predictions.add(new Prediction(5, 39, Prediction.GENDER_MALE));
        predictions.add(new Prediction(6, 39, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(7, 39, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(8, 39, Prediction.GENDER_MALE));
        predictions.add(new Prediction(9, 39, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(10, 39, Prediction.GENDER_MALE));
        predictions.add(new Prediction(11, 39, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(12, 39, Prediction.GENDER_FEMALE));

        predictions.add(new Prediction(1, 40, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(2, 40, Prediction.GENDER_MALE));
        predictions.add(new Prediction(3, 40, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(4, 40, Prediction.GENDER_MALE));
        predictions.add(new Prediction(5, 40, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(6, 40, Prediction.GENDER_MALE));
        predictions.add(new Prediction(7, 40, Prediction.GENDER_MALE));
        predictions.add(new Prediction(8, 40, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(9, 40, Prediction.GENDER_MALE));
        predictions.add(new Prediction(10, 40, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(11, 40, Prediction.GENDER_MALE));
        predictions.add(new Prediction(12, 40, Prediction.GENDER_FEMALE));

        predictions.add(new Prediction(1, 41, Prediction.GENDER_MALE));
        predictions.add(new Prediction(2, 41, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(3, 41, Prediction.GENDER_MALE));
        predictions.add(new Prediction(4, 41, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(5, 41, Prediction.GENDER_MALE));
        predictions.add(new Prediction(6, 41, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(7, 41, Prediction.GENDER_MALE));
        predictions.add(new Prediction(8, 41, Prediction.GENDER_MALE));
        predictions.add(new Prediction(9, 41, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(10, 41, Prediction.GENDER_MALE));
        predictions.add(new Prediction(11, 41, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(12, 41, Prediction.GENDER_MALE));

        predictions.add(new Prediction(1, 42, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(2, 42, Prediction.GENDER_MALE));
        predictions.add(new Prediction(3, 42, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(4, 42, Prediction.GENDER_MALE));
        predictions.add(new Prediction(5, 42, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(6, 42, Prediction.GENDER_MALE));
        predictions.add(new Prediction(7, 42, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(8, 42, Prediction.GENDER_MALE));
        predictions.add(new Prediction(9, 42, Prediction.GENDER_MALE));
        predictions.add(new Prediction(10, 42, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(11, 42, Prediction.GENDER_MALE));
        predictions.add(new Prediction(12, 42, Prediction.GENDER_FEMALE));

        predictions.add(new Prediction(1, 43, Prediction.GENDER_MALE));
        predictions.add(new Prediction(2, 43, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(3, 43, Prediction.GENDER_MALE));
        predictions.add(new Prediction(4, 43, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(5, 43, Prediction.GENDER_MALE));
        predictions.add(new Prediction(6, 43, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(7, 43, Prediction.GENDER_MALE));
        predictions.add(new Prediction(8, 43, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(9, 43, Prediction.GENDER_MALE));
        predictions.add(new Prediction(10, 43, Prediction.GENDER_MALE));
        predictions.add(new Prediction(11, 43, Prediction.GENDER_MALE));
        predictions.add(new Prediction(12, 43, Prediction.GENDER_MALE));

        predictions.add(new Prediction(1, 44, Prediction.GENDER_MALE));
        predictions.add(new Prediction(2, 44, Prediction.GENDER_MALE));
        predictions.add(new Prediction(3, 44, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(4, 44, Prediction.GENDER_MALE));
        predictions.add(new Prediction(5, 44, Prediction.GENDER_MALE));
        predictions.add(new Prediction(6, 44, Prediction.GENDER_MALE));
        predictions.add(new Prediction(7, 44, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(8, 44, Prediction.GENDER_MALE));
        predictions.add(new Prediction(9, 44, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(10, 44, Prediction.GENDER_MALE));
        predictions.add(new Prediction(11, 44, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(12, 44, Prediction.GENDER_FEMALE));

        predictions.add(new Prediction(1, 45, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(2, 45, Prediction.GENDER_MALE));
        predictions.add(new Prediction(3, 45, Prediction.GENDER_MALE));
        predictions.add(new Prediction(4, 45, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(5, 45, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(6, 45, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(7, 45, Prediction.GENDER_MALE));
        predictions.add(new Prediction(8, 45, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(9, 45, Prediction.GENDER_MALE));
        predictions.add(new Prediction(10, 45, Prediction.GENDER_FEMALE));
        predictions.add(new Prediction(11, 45, Prediction.GENDER_MALE));
        predictions.add(new Prediction(12, 45, Prediction.GENDER_MALE));

        return predictions;
    }
}
