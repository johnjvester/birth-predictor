package com.gitlab.johnjvester.birthpredictor.controllers;

import com.gitlab.johnjvester.birthpredictor.models.BirthPredictorRequest;
import com.gitlab.johnjvester.birthpredictor.models.Prediction;
import com.gitlab.johnjvester.birthpredictor.services.BirthPredictorService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@Tag(name = "Prediction API")
@RequiredArgsConstructor
@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
public class BirthPredictorController {
    private final BirthPredictorService birthPredictorService;

    @Operation(summary = "For a provided conceptionMonth and conceptionAge, returns a gender prediction.",
            description = "<ul>" +
                    "<li><strong>conceptionMonth</strong> accepts values from 1 to 12, corresponding with the month in which the child was conceived.</li>" +
                    "<li><strong>conceptionAge</strong> accepts values from 18 to 45, representing the age of the mother when the child was conceived.</li>" +
                    "</ul>")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "202", description = "Accepted"),
            @ApiResponse(responseCode = "400", description = "Bad Request"),
    })
    @PostMapping("/predict")
    public ResponseEntity<Prediction> predict(@RequestBody BirthPredictorRequest birthPredictorRequest) {
        log.info("predict(birthPredictorRequest={})", birthPredictorRequest);

        try {
            return new ResponseEntity<>(birthPredictorService.predict(birthPredictorRequest), HttpStatus.ACCEPTED);
        } catch (Exception e) {
            log.error("predict(birthPredictorRequest={}) error={}", birthPredictorRequest, e.getMessage(), e);
            return new ResponseEntity<>(new Prediction(birthPredictorRequest.getConceptionMonth(), birthPredictorRequest.getConceptionAge(), null, e.getMessage()), HttpStatus.BAD_REQUEST);
        }
    }
}
