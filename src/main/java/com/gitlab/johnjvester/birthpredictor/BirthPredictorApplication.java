package com.gitlab.johnjvester.birthpredictor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BirthPredictorApplication {

    public static void main(String[] args) {
        SpringApplication.run(BirthPredictorApplication.class, args);
    }

}
